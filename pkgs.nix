import (fetchTarball {
  url = "https://gitlab.com/samueltrose/ce-nixpkgs/-/archive/172459c2425c0f855c2ba12e6d3f645110aaa9bf/ce-nixpkgs-master.tar.gz";
  sha256 = "1fm9qk0pnqv6bakn3mslbb4x3gmz62g237viw7sn2kayipd649k1";
})
